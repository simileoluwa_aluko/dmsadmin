import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { RecordsService } from '../services/records.service';
import { Records } from '../interfaces/records'
import * as Config from '../config/config'

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.scss']
})
export class RecordsComponent implements OnInit {
  displayColumnStudent: string[] = ['position', 'firstName', 'lastName', 'otherNames', 'email', 'dateOfBirth', 'placeOfBirth', 'countryOfOrigin', 'stateOfOrigin', 'localGovtArea', 'address', 'phoneNumber', 'studentPassportPhoto']
  displayColumnFather: string[] = ['position', 'fg1firstName', 'fg1lastName', 'fg1Email', 'fg1PhoneNumberOne', 'fg1PhoneNumberTwo', 'fg1Address', 'fg1StateOfOrigin', 'fg1Occupation', 'fg1PassportPhoto']
  displayColumnMother: string[] = ['position', 'mg2firstName', 'mg2lastName', 'mg2Email', 'mg2PhoneNumberOne', 'mg2PhoneNumberTwo', 'mg2Address', 'mg2StateOfOrigin', 'mg2Occupation', 'mg2PassportPhoto']
  baseCloudinaryUrl = Config.Config.baseCloudinaryUrl
  dataSource = new MatTableDataSource<Records>(DATA);
  @ViewChild(MatPaginator) paginator: MatPaginator

  constructor(private recordsService: RecordsService) { }

  ngOnInit() {
    this.recordsService.getAllStudentsRecords().subscribe(response => {
      let index = 1
      let recordDataArray = []
      if (response.resStatus == 200) {
        response.StudentRecords.map(record => {
          record['record']['position'] = index
          index++
          let recordData = {}
          for (let item in record) {
            if(!record[item].studentPassportPhoto){
              record[item].studentPassportPhoto = 'dms/accounts/no_image'
            }
            if(!record[item].fathersPassportPhoto){
              record[item].fathersPassportPhoto = 'dms/accounts/no_image'
            }
            if(!record[item].mothersPassportPhoto){
              record[item].mothersPassportPhoto = 'dms/accounts/no_image'
            }
            if (item == 'record') {
              recordData = {
                position: record[item].position,
                firstName: record[item].firstName,
                lastName: record[item].lastName,
                email: record[item].email,
                otherNames: record[item].otherNames,
                Email: record[item].Email,
                dateOfBirth: record[item].dateOfBirth,
                placeOfBirth: record[item].placeOfBirth,
                countryOfOrigin: record[item].countryOfOrigin,
                stateOfOrigin: record[item].stateOfOrigin,
                localGovtArea: record[item].localGovtArea,
                Address: record[item].Address,
                phoneNumber: record[item].phoneNumber,
                studentPassportPhoto: record[item].studentPassportPhoto,
                regNumber: record[item].regNumber,
                fathersFirstName: record[item].fathersFirstName,
                fathersSurname: record[item].fathersSurname,
                fathersEmail: record[item].fathersEmail,
                fathersPhoneNumberOne: record[item].fathersPhoneNumberOne,
                fathersPhoneNumberTwo: record[item].fathersPhoneNumberTwo,
                fathersAddress: record[item].fathersAddress,
                fathersStateOfOrigin: record[item].fathersStateOfOrigin,
                fathersPassportPhoto: record[item].fathersPassportPhoto,
                fathersOccupation: record[item].fathersOccupation,
                mothersFirstName: record[item].mothersFirstName,
                mothersSurname: record[item].mothersSurname,
                mothersEmail: record[item].mothersEmail,
                mothersPhoneNumberOne: record[item].mothersPhoneNumberOne,
                mothersPhoneNumberTwo: record[item].mothersPhoneNumberTwo,
                mothersAddress: record[item].mothersAddress,
                mothersStateOfOrigin: record[item].mothersStateOfOrigin,
                mothersPassportPhoto: record[item].mothersPassportPhoto,
                mothersOccupation: record[item].mothersOccupation
              }
            }
          }
          recordDataArray.push(recordData)
        })
        this.dataSource = new MatTableDataSource<Records>(recordDataArray);
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

  openImage(publicId){
    let imageUrl = this.baseCloudinaryUrl + publicId
    window.open(imageUrl)
  }
}

const DATA: Records[] = []