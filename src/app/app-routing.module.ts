import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { AccountsComponent } from './accounts/accounts.component';
import { DocumentsComponent } from './documents/documents.component';
import { RecordsComponent } from './records/records.component';
import { ResultsComponent } from './results/results.component';
import { CreateAccountsComponent } from './create-accounts/create-accounts.component'

const routes: Routes = [
  {
    path : '',
    pathMatch : 'full',
    redirectTo : 'login'
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'dashboard',
    component : NavComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        component : AccountsComponent,
        outlet : 'nav'
      },
      {
        path : 'accounts',
        component : AccountsComponent,
        outlet : 'nav'
      },
      {
        path : 'documents',
        component : DocumentsComponent,
        outlet : 'nav'
      },
      {
        path : 'records',
        component : RecordsComponent,
        outlet : 'nav'
      },
      {
        path : 'results',
        component : ResultsComponent,
        outlet : 'nav'
      }, 
      {
        path : 'create-accounts',
        component : CreateAccountsComponent,
        outlet : 'nav'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
