export const port = 8000

export const Config = {
    loginRoute : `http://localhost:${port}/accounts/login/`,
    accountsRoute : `http://localhost:${port}/accounts/`,
    studentRoute : `http://localhost:${port}/students/`,
    necoRoute : `http://localhost:${port}/neco`,
    waecRoute : `http://localhost:${port}/waec/`,
    jambRoute : `http://localhost:${port}/jamb/`,
    appJSONContType : {'Content-Type' : 'application/json'},
    baseCloudinaryUrl : '//base cloudinary url'
}