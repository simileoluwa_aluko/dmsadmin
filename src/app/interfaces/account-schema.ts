export interface AccountSchema {
    position : number;
    firstName: string;
    lastName: string;
    email: string;
    course: string;
    college: string;
    regNumber: string;
    admissionYear: string;
    department: string;
    admissionStatus: string;
    studentAvatar: string;
    informationCompletionStatus: string;
}
