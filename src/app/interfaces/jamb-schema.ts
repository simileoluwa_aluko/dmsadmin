export interface JambSchema {
    position : number;
    surname : string;
    firstName : string;
    otherName : string; // or middle name
    gender : string;
    stateOfOrigin : string;
    localGovtArea : string; //or L.G.A.
    dateOfBirth : string;
    registrationNumber :string; 
    examinationNumber :string;
    examinationCentre : string;
    firstChoice :string;
    secondChoice :string;
    thirdChoice :string;
    fourthChoice :string;
    //subjects
    useOfEnglish : number;
    mathematics : number;
    physics : number;
    chemistry : number;
    biology : number;
    geography : number;
    literatureInEnglish : number;
    economics : number;
    commerce : number;
    accounts : number;
    government : number;
    christianReligiousKnowledge : number;
    agricScience : number;
    IslamicReligiousKnowledge : number;
    history : number;
    art : number;
    french : number;
    hausa : number;
    igbo : number;
    yoruba : number;
    music : number;
    fineArts : number;
}
