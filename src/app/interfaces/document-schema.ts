export interface DocumentSchema{
    position : number;
    firstName: string;
    lastName: string;
    email: string;
    regNumber : string,
    recommendationLetter? : string;
    jambAdmissionLetter? : string;
    birthCertificate? : string;
}