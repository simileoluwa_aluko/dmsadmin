export interface WaecSchema {
    position : number;
    name : string;
    examNumber : string;
    examination : string;
    centre : string;
    //subjects
    englishLanguage : number;
    generalMathematics : number;
    furtherMathematics : number;
    physics : number;
    chemistry : number;
    biology : number;
    geography : number;
    literatureInEnglish : number;
    economics : number;
    commerce : number;
    financialAccounting : number;
    government : number;
    christianReligiousStudies : number;
    agricScience : number;
    IslamicReligiousStudies: number;
    history : number;
    french : number;
    hausa : number;
    igbo : number;
    yoruba : number;
    music : number;
    fineArts : number;
    civicEducation : number;
}
