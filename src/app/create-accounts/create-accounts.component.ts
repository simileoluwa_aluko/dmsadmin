import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { CreateAccountService } from '../services/create-account.service'
import * as csvToJSON from 'csvtojson'

@Component({
  selector: 'app-create-accounts',
  templateUrl: './create-accounts.component.html',
  styleUrls: ['./create-accounts.component.scss']
})
export class CreateAccountsComponent implements OnInit {

  uploadMessage = ''
  createAccountForm: FormGroup
  accountCreationResponse = []

  constructor(private formBuilder: FormBuilder, private createAccountService: CreateAccountService) { }

  ngOnInit() {
    this.createAccountForm = this.formBuilder.group({
      accounts: this.formBuilder.array([])
    })
  }

  //TODO: add password and info completion status to account creation object

  /** CONVERT CSV FILE TO JSON */
  onFileChange($event) {
    let file: File = $event.target.files[0]

    if (!file || file.name.split('.').pop() != 'csv') {
      this.uploadMessage = 'File must be a csv file'
    } else {
      let fileReader = new FileReader()
      fileReader.readAsText(file)
      fileReader.onload = () => {
        csvToJSON().fromString(fileReader.result.toString())
          .then(json => {
            this.createAccountFromJSON(json)

          }, error => {
            this.uploadMessage = 'error processing csv'
          })
      }
    }
  }

  get accountForms() {
    return this.createAccountForm.get('accounts') as FormArray
  }

  deleteAccount(i) {
    this.accountForms.removeAt(i)
  }

  addAccount(firstName = '', lastName = '', email = '', course = '', college = '', regNumber = '', admissionYear = '', department = '', admissionStatus = '') {
    const account = this.formBuilder.group({
      firstName: [firstName, [Validators.required]],
      lastName: [lastName, [Validators.required]],
      email: [email, [Validators.required]],
      course: [course, [Validators.required]],
      college: [college, [Validators.required]],
      regNumber: [regNumber, [Validators.required]],
      admissionYear: [admissionYear, [Validators.required]],
      department: [department, [Validators.required]],
      admissionStatus: [admissionStatus, [Validators.required]]
    })
    this.accountForms.push(account)
  }

  createAccountFromJSON(json) {
    json.forEach(element => {
      this.addAccount(element['First Name'], element['Last Name'], element['Email'], element['Course'], element['College'], element['Registration Number'], element['Admission Year'], element['Department'], element['Admission Status'])
    })
  }

  onSubmit(data) {
    data['accounts'].forEach(element => {
      element['password'] = element['regNumber']
      element['informationCompletionStatus'] = 0
      this.createAccountService.createAccount(element).subscribe(response => {
          this.accountCreationResponse.push(`${response.message}`)
          while(this.accountForms.length != 0){
            this.accountForms.removeAt(0)
          }
      }, error => {
        this.accountCreationResponse.push(`creation of account for ${element['regNumber']} not successful`)
      })
    });
  }

  clearAccountCreationMessage(){
    this.ngOnInit()
    this.accountCreationResponse = []
  }
}