import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { MatBottomSheetModule, MatTabsModule,MatExpansionModule, MatPaginatorModule,MatTableModule, MatInputModule, MatTooltipModule, MatSelectModule, MatNativeDateModule, MatDatepickerModule, MatFormFieldModule, MatDividerModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule} from '@angular/material';
import { ReactiveFormsModule, FormsModule }   from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CloudinaryModule } from '@cloudinary/angular-5.x';
import * as  Cloudinary from 'cloudinary-core';

import { AppComponent } from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RecordsComponent } from './records/records.component';
import { AccountsComponent } from './accounts/accounts.component';
import { ResultsComponent } from './results/results.component';
import { DocumentsComponent } from './documents/documents.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { CreateAccountsComponent } from './create-accounts/create-accounts.component'

@NgModule({
  declarations: [
    AppComponent,
    RecordsComponent,
    AccountsComponent,
    ResultsComponent,
    DocumentsComponent,
    LoginComponent,
    NavComponent,
    CreateAccountsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatMenuModule,
    MatDividerModule, 
    MatTableModule,
    MatPaginatorModule,
    MatTabsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    HttpClientModule,
    MatSelectModule,
    LayoutModule,
    FormsModule,
    MatBottomSheetModule,
    CloudinaryModule.forRoot(Cloudinary, { cloud_name: 'simileoluwacloud'}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
