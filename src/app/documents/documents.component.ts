import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DocumentsService } from '../services/documents.service'
import { DocumentSchema } from '../interfaces/document-schema'
import * as Config from '../config/config'

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {
  displayedColumnsRecLet: string[] = ['position', 'firstName', 'lastName', 'regNumber', 'recommendationLetter']
  displayedColumnsJamb: string[] = ['position', 'firstName', 'lastName', 'regNumber', 'jambAdmissionLetter']
  displayedColumnsBirthCert: string[] = ['position', 'firstName', 'lastName', 'regNumber', 'birthCertificate']
  DATA = []
  baseCloudinaryUrl = Config.Config.baseCloudinaryUrl
  dataSource = new MatTableDataSource<DocumentSchema>(this.DATA)
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private documentsService: DocumentsService) { }

  ngOnInit() {
    this.documentsService.getAllStudentsRecords().subscribe(response => {
      let index = 1
      let documentDataArray = []
      if (response.resStatus == 200) {
        response.StudentRecords.map(record => {
          record['record']['position'] = index
          index++
          let documentData = {}
          for (let item in record) {
            if (item == 'record') {
              if(!record[item].recommendationLetter){
                record[item].recommendationLetter = 'dms/accounts/no_image'
              }
              if(!record[item].jambAdmissionLetter){
                record[item].jambAdmissionLetter = 'dms/accounts/no_image'
              }
              if(!record[item].birthCertificate){
                record[item].birthCertificate = 'dms/accounts/no_image'
              }
              documentData = {
                position: record[item].position,
                firstName: record[item].firstName,
                lastName: record[item].lastName,
                email: record[item].email,
                regNumber: record[item].regNumber,
                recommendationLetter: record[item].recommendationLetter,
                jambAdmissionLetter: record[item].jambAdmissionLetter,
                birthCertificate: record[item].birthCertificate
              }
            }
          }
          documentDataArray.push(documentData)
        })
        this.dataSource = new MatTableDataSource<DocumentSchema>(documentDataArray);
      }
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openImage(publicId){
    let imageUrl = this.baseCloudinaryUrl + publicId
    window.open(imageUrl)
  }

}

export interface studentSchema {
  firstName: string;
  lastName: string;
  otherNames: string;
  Email: string;
  dateOfBirth: string;
  placeOfBirth: string;
  countryOfOrigin: string;
  stateOfOrigin: string;
  localGovtArea: string;
  Address: string;
  phoneNumber: string;
  studentPassportPhoto: string;
  regNumber: string;
  fathersFirstName: string;
  fathersSurname: string;
  fathersEmail: string;
  fathersPhoneNumberOne: string;
  fathersPhoneNumberTwo: string;
  fathersAddress: string;
  fathersStateOfOrigin: string;
  fathersPassportPhoto: string;
  fathersOccupation: string;
  mothersFirstName: string;
  mothersSurname: string;
  mothersEmail: string;
  mothersPhoneNumberOne: string;
  mothersPhoneNumberTwo: string;
  mothersAddress: string;
  mothersStateOfOrigin: string;
  mothersPassportPhoto: string;
  mothersOccupation: string;

  //documents
  jambResultImage: string;
  waecResultImage: string;
  recommendationLetter: string;
  jambAdmissionLetter: string;
  birthCertificate: string;
}