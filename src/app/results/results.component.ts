import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ResultsService } from '../services/results.service';
import * as Config from '../config/config'

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  waecDataArray = []
  jambDataArray = []
  baseCloudinaryUrl = Config.Config.baseCloudinaryUrl
  displayedColumns: string[] = ['index', 'subject', 'grade'];
  @ViewChild(MatPaginator) paginator: MatPaginator

  constructor(private resultsService: ResultsService) { }

  ngOnInit() {
    
    this.resultsService.getJambRecords().subscribe(response => {
      let index = 1
      response.JambRecords.map(record => {
        record['record']['position'] = index
        if(!record['record']['image']){
          record['record']['image'] = 'dms/accounts/no_image'
        }
        index++
        this.jambDataArray.push(record['record'])
      })
    })

    this.resultsService.getWaecRecords().subscribe(response => {
      let index = 1
      response.WaecRecords.map(record => {
        record['record']['position'] = index
        if(!record['record']['image']){
          record['record']['image'] = 'dms/accounts/no_image'
        }
        index++
        let waecRecordObject = {}
        for (let i = 0; i < 9; i++) {
          
          let subject = record['record'].takenSubject[i]
          let grade = record['record'].grades[i]
          waecRecordObject[subject] = grade
        }
        record['record']['result'] = waecRecordObject
        this.waecDataArray.push(record['record'])
      })
      console.log(this.waecDataArray)
    })
  }
  openImage(publicId){
    let imageUrl = this.baseCloudinaryUrl + publicId
    window.open(imageUrl)
  }
}