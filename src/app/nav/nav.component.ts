import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
  section : string
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, private router: Router) { }

  ngOnInit(){
    let routerUrl = this.router.url
    if(routerUrl == '/dashboard'){
      this.section = '- Dashboard'
    }else{
      let sectionString = routerUrl.slice(routerUrl.indexOf(':') + 1, routerUrl.indexOf(')'))
      let formattedSectionString : string = sectionString.charAt(0).toUpperCase() + sectionString.slice(1)
      this.section = '- ' + formattedSectionString
    }
  }
  
  displayAccounts() {
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['accounts'] } }])
    this.section = '- Accounts'
  }
 
  displayDocuments() {
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['documents'] } }])
    this.section = '- Documents'
  }
 
  displayRecords() {
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['records'] } }])
    this.section = '- Records'
  }
 
  displayResults() {
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['results'] } }])
    this.section = '- Results'
  }

  displayaccountCreation(){
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['create-accounts'] } }])    
    this.section = '- Create Accounts'
  }
 
  logout() {
    this.router.navigate(['/login'])
  }
}