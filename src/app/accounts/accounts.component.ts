import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { AccountsService } from '../services/accounts.service'
import { AccountSchema } from '../interfaces/account-schema'
import { Router } from '@angular/router'
import * as Config from '../config/config'

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  TABLE_DATA: AccountSchema[] = []
  displayedColumns: string[] = ['position', 'firstName', 'lastName', 'email', 'course', 'college', 'regNumber', 'admissionYear', 'department', 'admissionStatus', 'studentAvatar', 'delete'];
  dataSource = new MatTableDataSource<AccountSchema>(this.TABLE_DATA)
  @ViewChild(MatPaginator) paginator: MatPaginator
  baseCloudinaryUrl = Config.Config.baseCloudinaryUrl


  constructor(private accountService: AccountsService, private router: Router, private bottomSheet: MatBottomSheet) { }

  ngOnInit() {
    let data = []
    this.dataSource.paginator = this.paginator;
    this.accountService.getAllAccounts().subscribe(response => {
      if (response.resStatus == 200) {
        let index = 1
        response.accounts.forEach(account => {
          account['account']['position'] = index
          if(!account['account']['studentAvatar']){
            account['account']['studentAvatar']='dms/accounts/no_image'
          }
          index++
          data.push(account['account'])
        })
        this.dataSource = new MatTableDataSource<AccountSchema>(data);
      }
    })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createAccounts() {
    this.router.navigate(['/dashboard', { outlets: { 'nav': ['create-accounts'] } }])
  }

  deleteAccount(regNumber: string) {
    this.accountService.deleteAccount(regNumber).subscribe(response => {
      this.ngOnInit()
    }, error => {
      alert(`account delete for ${regNumber} not successful. 
            message : ${error}`)
    })
  }

  openImage(publicId : string){
    let imageUrl = this.baseCloudinaryUrl + publicId
    window.open(imageUrl)
  }
}