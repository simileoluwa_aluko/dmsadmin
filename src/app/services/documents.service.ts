import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class DocumentsService {

  baseStudentURL = Config.Config.studentRoute

  constructor(private http : HttpClient) { 
  }

  getAllStudentsRecords(){
    return this.http.get<responseSchema>(this.baseStudentURL, {headers : {'Content-Type' : 'application/json'}})
  }

}
export interface responseSchema {
  resStatus : number;
  count : number;
  StudentRecords : Object[];
}