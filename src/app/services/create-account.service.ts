import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class CreateAccountService {
  baseAccountUrl = Config.Config.accountsRoute

  constructor(private httpClient : HttpClient) { }

  createAccount(data){
    let url = this.baseAccountUrl + 'create-account'
    return this.httpClient.post<responseSchema>(url, data, {headers : { 'Content-Type': 'application/json' }})
  }
}

export interface responseSchema {
  resStatus : number;
  message? : string;
  error? : string;
}