import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  accountUrl = Config.Config.accountsRoute

  constructor(private http: HttpClient) { }

  getAllAccounts() {
    
    return this.http.get<responseSchema>(this.accountUrl, { headers: { 'Content-Type': 'application/json' } })
  }

  getAnAccount(regNumber: string) {
    return this.http.get(this.accountUrl + regNumber, { headers: { 'Content-Type': 'application/json' } })
  }

  deleteAccount(regNumber : string){
    return this.http.delete(this.accountUrl + regNumber, { headers: { 'Content-Type': 'application/json' }})
  }
}

export interface responseSchema {
  resStatus: number;
  count?: number;
  accounts?: Object[];
  account?: Object;
  message?: string;
  requests?: string;
  url?: string;
  error?: string;
}
