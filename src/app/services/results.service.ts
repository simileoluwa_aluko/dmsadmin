import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class ResultsService {
  baseJambUrl = Config.Config.jambRoute
  baseWaecUrl = Config.Config.waecRoute
  baseNecoUrl = Config.Config.necoRoute

  constructor(private http : HttpClient) { }
  
  getJambRecords(){
    return this.http.get<jambResponseSchema>(this.baseJambUrl, {headers :  {'Content-Type' : 'application/json'}})
  }
  getWaecRecords(){
    return this.http.get<waecResponseSchema>(this.baseWaecUrl, {headers :  {'Content-Type' : 'application/json'}})
  }
  getNecoRecords(){
    return this.http.get<NecoResponseSchema>(this.baseJambUrl, {headers :  {'Content-Type' : 'application/json'}})
  }
}

export interface jambResponseSchema{
  resStatus :number;
  count : number;
  JambRecords : Object[];
}
export interface waecResponseSchema{
  resStatus :number;
  count : number;
  WaecRecords : Object[];
}
export interface NecoResponseSchema{
  resStatus :number;
  count : number;
  NecoRecords : Object[];
}
